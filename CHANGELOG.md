# Auto Semver CHANGELOG

## 1.2.0 [03-10-2018]

- Add JSHint and configure for ES6
- Fix errors found by linter in `src/increment/incrementer.js`

## 1.1.0 [03-10-2018]

- Write rudimentary logic for handling version bumping

## 1.0.1 [03-10-2018]

- Add placeholder files for development

## 1.0.0 [03-10-2018]

- Create project
- Add README
- Initialize package.json