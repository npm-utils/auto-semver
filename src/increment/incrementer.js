"use strict";

var packageJson = require('../../package.json');
const configuration = {
  branch: 3
};

function readPackageVersion(packageJson) {
  console.log(packageJson.version);
}

function increment(packageJson) {
  if (configuration.branch === 1) {
    packageJson.version = incrementMajor(packageJson.version);
  } else if (configuration.branch === 2) {
    packageJson.version = incrementMinor(packageJson.version);
  } else if (configuration.branch === 3) {
    packageJson.version = incrementPatch(packageJson.version);
  } else {
    throw new Error();
  }
}

function incrementMajor(version) {
  let sv = splitVersion(version);
  sv[0]++;
  sv = joinVersion(sv);
  console.log(sv);
}

function incrementMinor(version) {
  let sv = splitVersion(version);
  sv[1]++;
  sv = joinVersion(sv);
  console.log(sv);
}

function incrementPatch(version) {
  let sv = splitVersion(version);
  sv[2]++;
  sv = joinVersion(sv);
  console.log(sv);
}

function splitVersion(version) {
  return version.split(".");
}

function joinVersion(version) {
  return version.join(".");
}

increment(packageJson);