# Auto Semver

Auto Semver automatically increments the package.json and package-lock.json 
based on the branch that the developer is working on, in accordance with
the 12-factor application.
